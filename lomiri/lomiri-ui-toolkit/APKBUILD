# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=lomiri-ui-toolkit
pkgver=0_git20211223
_commit="7a596d16132cae5972fa4a4b1eac2ea7bbfda52c"
pkgrel=0
pkgdesc='The Ubuntu UI toolkit used in Ubuntu Touch'
arch="all"
url="https://gitlab.com/ubports/core/lomiri-ui-toolkit"
license="LGPL-3.0-only"
depends="qt5-qtfeedback qt5-qtgraphicaleffects"
depends_dev="libnih-dev qt5-qtsvg-dev qt5-qtpim-dev qt5-qtsystems-dev lttng-ust-dev libxi-dev eudev-dev mir-dev libevdev-dev qt5-qtgraphicaleffects qt5-qtfeedback-dev"
makedepends="$depends_dev qt5-qttools-dev qt5-qtdeclarative-dev"
checkdepends="diffutils bash grep xvfb-run dbus-test-runner"
source="https://gitlab.com/ubports/core/lomiri-ui-toolkit/-/archive/$_commit/lomiri-ui-toolkit-$_commit.tar.gz
	0001-Remove-debian-isms-from-test-runner.patch
	0002-Don-t-build-app-launch-profiler.patch
	"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # several tests fail
builddir="$srcdir/$pkgname-$_commit"

build() {
	qmake-qt5 CONFIG+=no_docs
	make
}

check() {
	xvfb-run make check
}

package() {
	make INSTALL_ROOT="$pkgdir" install
}

sha512sums="
7cde23083ca815dc0b558f7f3409c924f6cd6ef1981409e73291625198d20694a7e79371c63461e7cfcf39674a1709274850d4543cc518c48a75a13024182d20  lomiri-ui-toolkit-7a596d16132cae5972fa4a4b1eac2ea7bbfda52c.tar.gz
7423ea0f4edececdeab09c4c91686e981ecedc2b7b39d147d604eb1bd8cdb26a1c3b4fad848215a1a8db1197bd3a58fb82651b0e298c52181632ae629c307d9a  0001-Remove-debian-isms-from-test-runner.patch
46a12139eeb09b4a7baba4c020103bff8c7885d65fc88ec2e2f4cbf6d93f0aed8d853da316619cb3bafdde5853517dab0b372132d91a03870b9e06a839a854df  0002-Don-t-build-app-launch-profiler.patch
"
