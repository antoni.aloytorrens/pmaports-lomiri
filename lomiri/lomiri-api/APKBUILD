# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=lomiri-api
pkgver=0_git20211126
_commit="105f7ec662ff40910fd74a86022f1c7772344497"
pkgrel=0
pkgdesc="API for Unity shell integration"
url="https://gitlab.com/ubports/core/lomiri-api"
arch="all"
license="LGPL-3.0"
makedepends="cmake cmake-extras qt5-qtdeclarative-dev libqtdbustest gtest-dev gmock"
checkdepends="dbus"
source="https://gitlab.com/ubports/core/lomiri-api/-/archive/$_commit/lomiri-api-$_commit.tar.gz
	qmltestrunner.patch"
subpackages="$pkgname-dev"
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	make -C build
}

check() {
	cd build
	# GioMemory_test fails under QEMU for aarch64
	# Error connecting to D-Bus address unix:abstract=/tmp/dbus-IRpDQKgJCX,guid=daa250871edc3ad426e54ceb5e9b5bde: Error sending credentials: Error sending message: No buffer space available
	env CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "GioMemory_test"
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="
5359ddc71060bf6997a6cba35c5110527525bffcfd541bd4286c32bb87d34a2e28b1041d0c5583c76b51ed58f9d0c6c2d8c7788d89aab85869db9281971806fa  lomiri-api-105f7ec662ff40910fd74a86022f1c7772344497.tar.gz
04c316667ed5892c55278b01377d4d9c1a6ae14a330bfa594e5e54371d37441747684ab553799cb5e7804f4c68d181e78caeb8279491b9f5302a4e70095c74af  qmltestrunner.patch
"
